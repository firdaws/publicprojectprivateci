# PublicProjectPrivateCI

This is a test project to test and confirm the [effect of the `Public Access` Setting in CI/CD](https://docs.gitlab.com/ee/ci/pipelines/settings.html#change-which-users-can-view-your-pipelines) for different users with different roles.

Feel free to add yourself and test!

<br/>

## `Public Access` Setting in Project:

![CI_CD_Public_Access](./CI_CD_Public_Access.png)


<br/>

## Visibility Status Check

#### Project Member with `Guest` Role

1. [ ] CI/CD Menu  
1. [ ] Pipelines Page  
1. [ ] Jobs list  
1. [ ] Download artifacts  
1. [ ] Job output logs  
1. [ ] Pipeline security dashboard  
 
#### Project Member with `Reporter` Role or Higher

1. [ ] CI/CD Menu  
1. [ ] Pipelines Page  
1. [ ] Jobs list  
1. [ ] Download artifacts  
1. [ ] Job output logs  
1. [ ] Pipeline security dashboard 

#### GitLab logged in user

1. [ ] CI/CD Menu  
1. [ ] Pipelines Page  
1. [ ] Jobs list  
1. [ ] Download artifacts  
1. [ ] Job output logs  
1. [ ] Pipeline security dashboard 

#### Public user (Incognito browser)

1. [ ] CI/CD Menu  
1. [ ] Pipelines Page  
1. [ ] Jobs list  
1. [ ] Download artifacts  
1. [ ] Job output logs  
1. [ ] Pipeline security dashboard 
